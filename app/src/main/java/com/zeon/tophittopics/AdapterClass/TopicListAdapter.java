package com.zeon.tophittopics.AdapterClass;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.zeon.tophittopics.AsyncTask.CommonAsyncTask;
import com.zeon.tophittopics.BaseClass.Topics;
import com.zeon.tophittopics.R;
import com.zeon.tophittopics.Utils.CustomProcessDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import cz.msebera.android.httpclient.Header;

public class TopicListAdapter extends RecyclerView.Adapter<TopicListAdapter.ViewHolder> {

    private Context mContext;
    private List<Topics> topicList;
    CustomProcessDialog customProcessDialog;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView topicTitle, upvote, downvote;
        public LinearLayout upvoteLinear, downvoteLinear;

        public ViewHolder(View view) {
            super(view);

            topicTitle  = (TextView) view.findViewById(R.id.items_topic_title);
            upvote      = (TextView) view.findViewById(R.id.items_topic_upvote_count);
            downvote    = (TextView) view.findViewById(R.id.items_topic_downvote_count);

            upvoteLinear        = view.findViewById(R.id.items_topic_upvote);
            downvoteLinear      = view.findViewById(R.id.items_topic_downvote);

        }
    }


    public TopicListAdapter(Context mContext, List<Topics> topicList) {
        this.mContext = mContext;
        this.topicList = topicList;
        customProcessDialog = new CustomProcessDialog(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.items_topic_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final Topics topics = topicList.get(position);


        holder.topicTitle.setText(topics.getTopicName());
        holder.upvote.setText(topics.getUpvote());
        holder.downvote.setText(topics.getDownvote());

        holder.upvoteLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voteTopic(topics.getId(), CommonAsyncTask.TOPIC_VOTE_UP, position, holder.upvote);
            }
        });

        holder.downvoteLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voteTopic(topics.getId(), CommonAsyncTask.TOPIC_VOTE_DOWN, position, holder.downvote);
            }
        });

    }


    @Override
    public int getItemCount() {
        return topicList.size();
    }


    /*
     * Update topic vote to server
     */
    public void voteTopic(String topicId, final String vote, final int position, final TextView voteTextview) {
        final CommonAsyncTask commonAsyncTask = new CommonAsyncTask();
        commonAsyncTask.updateTopicsVote(topicId, vote, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                customProcessDialog.showProcessDialog();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                customProcessDialog.dismissProcessDialog();
                try {
                    if(response.getBoolean("SUCCESS")) {
                        // When success update to server, update local value & view
                        if(vote.equals(CommonAsyncTask.TOPIC_VOTE_UP)) {
                            topicList.get(position).addUpvote();
                            voteTextview.setText(topicList.get(position).getUpvote());
                        } else {
                            topicList.get(position).addDownvote();
                            voteTextview.setText(topicList.get(position).getDownvote());
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                customProcessDialog.dismissProcessDialog();
            }
        });

    }


}

package com.zeon.tophittopics.AsyncTask;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CommonAsyncTask {

    public static final String TAG = "CommonAsyncTask"; // Used for debug tag

    AsyncHttpClient asyncHttpClient;
    RequestParams requestParams;

    final private String BASE_URL           = "https://top-hits-topics.herokuapp.com/"; // Base api url


    final public static String TOPIC_VOTE_UP   = "0";
    final public static String TOPIC_VOTE_DOWN = "1";



    public CommonAsyncTask() {
        asyncHttpClient = new AsyncHttpClient();
        requestParams = new RequestParams();
    }

    // Retrieve topics from server
    public void retrieveTopics(JsonHttpResponseHandler handler) {
        // Make sure requestParams is set to empty
        requestParams = new RequestParams();

        requestParams.put("tag", "topics");

        asyncHttpClient.get(BASE_URL, requestParams, handler);
    }


    // Retrieve top 20 topics from server
    public void retrieveTopTopics(JsonHttpResponseHandler handler) {
        // Make sure requestParams is set to empty
        requestParams = new RequestParams();

        requestParams.put("tag", "tophits");
        requestParams.put("max_item", 20);

        asyncHttpClient.get(BASE_URL, requestParams, handler);
    }


    // Update vote topics to server
    public void updateTopicsVote(String topicId, String vote, JsonHttpResponseHandler handler) {
        // Make sure requestParams is set to empty
        requestParams = new RequestParams();

        requestParams.put("tag", "vote");
        requestParams.put("topic_id", topicId);
        requestParams.put("vote_type", vote);

        asyncHttpClient.get(BASE_URL, requestParams, handler);
    }


    // Submit new topics to server
    public void submitTopics(String newTopic, JsonHttpResponseHandler handler) {
        // Make sure requestParams is set to empty
        requestParams = new RequestParams();

        requestParams.put("tag", "submittopic");
        requestParams.put("topic", newTopic);

        asyncHttpClient.get(BASE_URL, requestParams, handler);
    }


}

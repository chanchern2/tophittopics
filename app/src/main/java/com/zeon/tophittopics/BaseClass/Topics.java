package com.zeon.tophittopics.BaseClass;




public class Topics {

    private String topic_id;
    private String topic_name;
    private String upvote;
    private String downvote;


    public Topics() {
    }

    public Topics(String topic_id, String topic_name, String upvote, String downvote) {
        this.topic_id = topic_id;
        this.topic_name = topic_name;
        this.upvote = upvote;
        this.downvote = downvote;

    }

    public String getId() {
        return topic_id;
    }

    public void setId(String topic_id) {
        this.topic_id = topic_id;
    }

    public String getTopicName() {
        return topic_name;
    }

    public void setTopicName(String topic_name) {
        this.topic_name = topic_name;
    }

    public String getUpvote() {
        return upvote;
    }

    public void setUpvote(String upvote) {
        this.upvote = upvote;
    }

    public void addUpvote() {
        this.upvote = String.valueOf(Integer.parseInt(this.upvote) + 1);
    }

    public String getDownvote() {
        return downvote;
    }

    public void setDownvote(String downvote) {
        this.downvote = downvote;
    }

    public void addDownvote() {
        this.downvote = String.valueOf(Integer.parseInt(this.downvote) + 1);
    }
}

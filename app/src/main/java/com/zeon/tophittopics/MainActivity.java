package com.zeon.tophittopics;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.zeon.tophittopics.AdapterClass.TopicListAdapter;
import com.zeon.tophittopics.AsyncTask.CommonAsyncTask;
import com.zeon.tophittopics.BaseClass.Topics;
import com.zeon.tophittopics.Utils.CommonUtils;
import com.zeon.tophittopics.Utils.CustomProcessDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    CustomProcessDialog customProcessDialog;

    private CommonAsyncTask commonAsyncTask;
    AlertDialog createTopicDialog;

    TextView topTopicTitle;

    private SwipeRefreshLayout pullToRefreshView;
    private RecyclerView recyclerView;
    private TopicListAdapter topicListAdapter;
    private List<Topics> topicList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customProcessDialog = new CustomProcessDialog(this);

        commonAsyncTask = new CommonAsyncTask(); // Declare new async task for later use to connect api

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Floating button for user to add new topic
        FloatingActionButton fabAddTopic = (FloatingActionButton) findViewById(R.id.fab_add_topics);
        fabAddTopic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createNewTopic();
            }
        });


        topTopicTitle   = findViewById(R.id.main_view_title);


        /*
         * Setup recycler view to handle top topic list
         */
        recyclerView        = (RecyclerView) findViewById(R.id.main_view_recycler);
        topicList           = new ArrayList<>();
        topicListAdapter    = new TopicListAdapter(this, topicList);
        pullToRefreshView   = (SwipeRefreshLayout) findViewById(R.id.main_view_pull_refresh);
        pullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveTopTopic();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(topicListAdapter);

        retrieveTopTopic();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_all) {
            Intent topicsActivity = new Intent(this, TopicsActivity.class);
            this.startActivity(topicsActivity);
        }

        return super.onOptionsItemSelected(item);
    }


    /*
     * Ready & fetch top 20 topics from database
     */
    protected void retrieveTopTopic() {
        topicList.clear();

        // connect api to get top 20 topics
        commonAsyncTask.retrieveTopTopics(new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pullToRefreshView.setRefreshing(true);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pullToRefreshView.setRefreshing(false);
                try {
                    int response_length = response.length();
                    if(response_length != 0) {

                        if(response.getBoolean("SUCCESS")) {
                            JSONArray responseArray = response.getJSONArray("RESPONSE");

                            for(int i=0; i<responseArray.length(); i++) {
                                Topics topicContent;

                                String topicId      = responseArray.getJSONObject(i).getString("topics_id");
                                String topicName    = responseArray.getJSONObject(i).getString("topic_name");
                                String upVote       = responseArray.getJSONObject(i).getString("upvote");
                                String downVote     = responseArray.getJSONObject(i).getString("downvote");

                                topicContent = new Topics(topicId, topicName, upVote, downVote); // put data into topic class prepare to store into list
                                topicList.add(topicContent); // add topic content into list to show in view
                            }
                            topicListAdapter.notifyDataSetChanged();
                        } else {
                            new android.app.AlertDialog.Builder(MainActivity.this)
                                    .setMessage(response.getString("ERROR"))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .create().show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pullToRefreshView.setRefreshing(false);
            }
        });
    }


    /*
     * Show dialog for user to create new topic
     */
    protected void createNewTopic() {
        final Button submitBtn, cancelBtn;
        final EditText topicText;

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);  // ready to build an alert dialog form
        LayoutInflater inflater = this.getLayoutInflater(); // Ready inflater to inflate custom layout to show form in dialog
        final View dialogForm = inflater.inflate(R.layout.dialog_submit_topic, null); // inflate custom layout as view

        topicText   = dialogForm.findViewById(R.id.dialog_submit_topic_text);   // To allow user to enter new topic

        builder.setView(dialogForm)
                .setCancelable(false);

        createTopicDialog = builder.create();
        createTopicDialog.show();


        submitBtn = (Button) dialogForm.findViewById(R.id.dialog_submit_topic_action_submit);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Reset errors.
                topicText.setError(null);

                boolean cancelProcess = false;
                View focusView = null;

                // Validation check on all input
                if (!CommonUtils.hasText(topicText)) {
                    focusView = topicText;
                    cancelProcess = true;
                }

                if (cancelProcess) {
                    // There was an error, don't attempt login and focus the first form field with an error.
                    focusView.requestFocus();
                } else {
                    submitNewTopic(topicText.getText().toString());
                }
            }
        });
        cancelBtn = (Button) dialogForm.findViewById(R.id.dialog_submit_topic_action_cancel);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createTopicDialog.dismiss();
            }
        });

    }



    /*
     * Submit new topic to server
     * topic : newly entered topic by user
     */
    protected void submitNewTopic(String topic) {

        // submit new topic to server
        commonAsyncTask.submitTopics(topic, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                customProcessDialog.showProcessDialog();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                customProcessDialog.dismissProcessDialog();
                try {
                    int response_length = response.length();
                    if(response_length != 0) {

                        if(response.getBoolean("SUCCESS")) {
                            createTopicDialog.dismiss();
                            retrieveTopTopic();
                        } else {
                            new android.app.AlertDialog.Builder(MainActivity.this)
                                    .setMessage(response.getString("ERROR"))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .create().show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                customProcessDialog.dismissProcessDialog();
            }
        });
    }



}

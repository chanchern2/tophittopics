package com.zeon.tophittopics;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.zeon.tophittopics.AdapterClass.TopicListAdapter;
import com.zeon.tophittopics.AsyncTask.CommonAsyncTask;
import com.zeon.tophittopics.BaseClass.Topics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class TopicsActivity extends AppCompatActivity {

    private CommonAsyncTask commonAsyncTask;

    private SwipeRefreshLayout pullToRefreshView;
    private RecyclerView recyclerView;
    private TopicListAdapter topicListAdapter;
    private List<Topics> topicList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics);

        commonAsyncTask = new CommonAsyncTask(); // Declare new async task for later use to connect api


        /*
         * Setup recycler view to handle topic list
         */
        recyclerView        = (RecyclerView) findViewById(R.id.topics_view_recycler);
        topicList           = new ArrayList<>();
        topicListAdapter    = new TopicListAdapter(this, topicList);
        pullToRefreshView   = (SwipeRefreshLayout) findViewById(R.id.topic_list_pull_refresh);
        pullToRefreshView.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                retrieveTopic();
            }
        });

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(topicListAdapter);

        retrieveTopic();
    }


    /*
     * Ready & fetch topics from database
     *
     */
    protected void retrieveTopic() {
        topicList.clear();

        // connect api to get all topics
        commonAsyncTask.retrieveTopics(new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                pullToRefreshView.setRefreshing(true);
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                pullToRefreshView.setRefreshing(false);
                try {
                    int response_length = response.length();
                    if(response_length != 0) {

                        if(response.getBoolean("SUCCESS")) {
                            JSONArray responseArray = response.getJSONArray("RESPONSE");

                            for(int i=0; i<responseArray.length(); i++) {
                                Topics topicContent;

                                String topicId      = responseArray.getJSONObject(i).getString("topics_id");
                                String topicName    = responseArray.getJSONObject(i).getString("topic_name");
                                String upVote       = responseArray.getJSONObject(i).getString("upvote");
                                String downVote     = responseArray.getJSONObject(i).getString("downvote");

                                topicContent = new Topics(topicId, topicName, upVote, downVote); // put data into topic class prepare to store into list
                                topicList.add(topicContent); // add topic content into list to show in view
                            }
                            topicListAdapter.notifyDataSetChanged();
                        } else {
                            new android.app.AlertDialog.Builder(TopicsActivity.this)
                                    .setMessage(response.getString("ERROR"))
                                    .setCancelable(false)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    })
                                    .create().show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                pullToRefreshView.setRefreshing(false);
            }
        });

    }

}

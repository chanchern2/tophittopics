package com.zeon.tophittopics.Utils;


import android.widget.EditText;

public class CommonUtils {


    /*
     * Check edit text field is empty
     */
    public static boolean hasText(EditText editText) {

        String text = editText.getText().toString().trim();
        editText.setError(null);

        // length 0 means there is no text
        if (text.length() == 0) {
            editText.setError("Field required");
            return false;
        }

        return true;
    }
}

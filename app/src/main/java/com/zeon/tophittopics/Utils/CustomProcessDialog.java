package com.zeon.tophittopics.Utils;


import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.zeon.tophittopics.R;


public class CustomProcessDialog {

    Dialog dialog;
    ImageView mainLayout;
    Animation rotation;

    public CustomProcessDialog(Context context) {

        final View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_on_progress, null);

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        mainLayout = (ImageView) dialogView.findViewById(R.id.dialog_progress_icon);
        rotation = AnimationUtils.loadAnimation(context, R.anim.progress_rotation);

    }

    public void showProcessDialog() {
        mainLayout.startAnimation(rotation);
        dialog.show();
    }

    public void dismissProcessDialog() {
        dialog.dismiss();
    }
}
